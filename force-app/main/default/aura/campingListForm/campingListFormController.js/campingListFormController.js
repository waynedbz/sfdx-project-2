({
	clickCreateItem : function(component, event, helper) {
		var validCampItem = component.find('campingform').reduce(function (validSoFar, inputCmp) {
            // Displays error messages for invalid fields
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        
        if(validCampItem){
            
            
            var newItem = component.get("v.newItem");
            
            //var newCampItem = JSON.parse(JSON.stringify(newItem));
            
            helper.createItem(component,newItem);
        }
    }
})