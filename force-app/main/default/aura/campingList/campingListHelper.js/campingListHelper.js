({
	createItem : function(component, newItem) {
		var action = component.get("c.saveItem");
            action.setParams({
                "campingitem": newItem
            });
            
            action.setCallback(this, function(response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    
                    var theCampItems = component.get("v.items");
                    theCampItems.push(response.getReturnValue());
                    component.set("v.newItem",{'sobjectType':'Camping_Item__c',
                                               'Name': '',
                                               'Quantity__c': 0,
                                               'Price__c': 0,
                                               'Packed__c': false});
                    
                    component.set("v.items", theCampItems);
                    
                }
            });
            
            $A.enqueueAction(action);
	}
})