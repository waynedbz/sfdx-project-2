({
    handleClick: function(c, e, h) {
        var btnClicked = e.getSource();         // the button
        console.log('btnClicked '+btnClicked);
        var btnMessage = btnClicked.get("v.label"); // the button's label
        c.set("v.message", btnMessage);     // update our message
    }
})