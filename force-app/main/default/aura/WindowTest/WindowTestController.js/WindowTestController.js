({
    doInit: function(component, event, helper) {
        helper.preventLeaving();
    },
    afterSave: function(component, event, helper) {
        helper.allowLeaving();
    }
})