({
	handleClick : function(component, event, helper) {
		var action = component.get("c.getName");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success: Sent for approval!",
                    type: 'success',
                    "message": "Your request has been sent for approval."
                });
                toastEvent.fire();

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "mode": "sticky",
                            "type": "error",
                            "title": "Error",
                            "message": errors[0].message
                        });
                        toastEvent.fire();
                    }
                } else {
                    console.log("Unknown error");

                }
            }
        });
        $A.enqueueAction(action);
	}
})