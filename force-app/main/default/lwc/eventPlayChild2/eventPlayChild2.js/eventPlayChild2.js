/* eslint-disable no-console */
import { LightningElement,track, wire } from 'lwc';
import {CurrentPageReference} from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

export default class EventPlayChild2 extends LightningElement {
    @track msg;
    @wire(CurrentPageReference) pageRef

    handleChange(event) {
        this.msg = event.target.value;
    }

    handleClick() {
        fireEvent(this.pageRef,'handleChange',this.msg);
    }
}