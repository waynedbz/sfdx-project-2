/* eslint-disable no-console */
/* eslint-disable no-alert */
import {LightningElement,track, wire} from 'lwc';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import {CurrentPageReference} from 'lightning/navigation';

export default class EventPlayChild1 extends LightningElement {
    @track msg1;
    @track msg;
    @wire(CurrentPageReference) pageRef;

    handleChange(event) {
        this.msg1 = event.target.value;
    }

    handleClick() {
        this.dispatchEvent(new CustomEvent('sendmsg',{detail:this.msg1}));
    }

    connectedCallback(){
        registerListener('handleChange', this.handleEvent,this);
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
      }
     

    handleEvent(msg){
        alert(`handled from c1 - ${msg}`);
    }
}