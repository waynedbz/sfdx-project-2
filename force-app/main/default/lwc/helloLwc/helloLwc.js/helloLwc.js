import { LightningElement, track} from 'lwc';

export default class HelloLwc extends LightningElement {
    @track message1 = "1. The lwc helloword";
    @track message2 = "2. The lwc helloword";

    handleInput1(event){
        this.message1 = event.target.value;
        console.log(event.target.value);
        console.log(event.target.name);
        console.log(event.target.dataset.id);
        console.log(event.target.dataset.it);
    }

    handleInput2(event){
        this.message2 = event.target.value;
        console.log(event.target.value);
        console.log(event.target.name);
        console.log(event.target.dataset.id);
        console.log(event.target.dataset.it);
    }

}