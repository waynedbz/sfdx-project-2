import { LightningElement, track} from 'lwc';

export default class HelloLwc2 extends LightningElement {
    @track message1 = "1. The lwc helloword";

    handleInput1(event){
        this.message1 = event.target.value;
        console.log(event.target.value);
        console.log(event.target.name);
        console.log(event.target.dataset.id);
        console.log(event.target.dataset.it);
    }

}