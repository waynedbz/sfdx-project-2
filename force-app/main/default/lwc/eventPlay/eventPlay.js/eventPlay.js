/* eslint-disable no-console */
/* eslint-disable no-alert */
import { LightningElement, track, api, wire } from 'lwc';
import {getRecord, getFieldValue} from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSomeAccounts from '@salesforce/apex/EventPlayController.getSomeAccounts';

import NAME_FIELD from '@salesforce/schema/Account.Name';
import OWNER_NAME_FIELD from '@salesforce/schema/Account.Owner.Name';
import LWC_TEST_FIELD from '@salesforce/schema/Account.Lwc_test__c';

const fields = [NAME_FIELD, LWC_TEST_FIELD];
const optionalFields = [OWNER_NAME_FIELD];


const columns = [
    { label: 'Id', fieldName: 'Id' },
    { label: 'Name', fieldName: 'Name'},
    { label: 'Lwc Test', fieldName: 'Lwc_test__c' },
    { label: 'Type', fieldName: 'Type' }
];

export default class EventPlay extends LightningElement {
    
    @track msg;
    @track account = {};
    @track accounts = [];
    @track columns = columns;

    @api recordId;
    @wire(getRecord, { recordId: '0012400001C4X3iAAF', fields, optionalFields}) 
    accountFn({ error, data }) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading contact',
                    message,
                    variant: 'error',
                }),
            );
        } else if (data) {
           this.account.name = getFieldValue(data,NAME_FIELD);
           this.account.owner = getFieldValue(data,OWNER_NAME_FIELD);
           this.account.lwc_test = getFieldValue(data,LWC_TEST_FIELD);

           this.dispatchEvent(
                new ShowToastEvent({
                    title: 'NO Error loading account',
                    message: 'Yipee',
                    variant: 'Success',
                }),
            );
        }
    }
    
    @wire(getSomeAccounts)
    wiredAccounts({ error, data }) {
            if (data) {
                this.accounts = data;
                this.error = undefined;
            } else if (error) {
                this.error = error;
                this.accounts = undefined;
            }
        }

    handleSendMsg(event){
        alert(`Parent handleSendMsg ${event.detail}`);
    }

}