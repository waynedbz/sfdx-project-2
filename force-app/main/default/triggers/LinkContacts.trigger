trigger LinkContacts on Opportunity (after insert) 
{
    
    //firstly, the set of unique Account Id's related to the opportunities are collected
    Set<ID> accountIds = new Set<ID>();
    for(Opportunity opt:Trigger.New){
        accountIds.add(opt.AccountId);
    }
    
    //get all contacts for those accounts which are flagged as key contacts using the checkbox field
    list<Contact> contacts = new list<Contact>();
    contacts = [select id, AccountId from Contact where AccountId in: accountIds AND Key_Contact__c = true];
    system.debug('contact list:' + contacts.size());
    //Create a collection of account id's with their corresponding contacts
    Map<Id,List<Contact>> contactsByAccount = new Map<ID,List<Contact>>();
    for(Contact c:contacts){
        if(contactsByAccount.get(c.AccountId) == null){
            contactsByAccount.put(c.AccountId,new List<Contact>());
        }
        contactsByAccount.get(c.AccountId).add(c);
    }
    
    // check to see if the Opportunity already has a contact role.  If it does, add to a set of Ids to exclude
    List<OpportunityContactRole> existingOCR = new List<OpportunityContactRole>();
    Set<Id> existingOCRIds = new Set<Id>();
    

    
    //create the OpportunityContactRole objects
    list<OpportunityContactRole> listOCR = new list<OpportunityContactRole>();
    for(Opportunity opt:Trigger.New){
        if(!existingOCRIds.contains(opt.Id) && contactsByAccount.get(opt.AccountId) != null){
            Boolean firstContact = true;
            for(Contact c: contactsByAccount.get(opt.AccountId))
            {
                OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId=opt.id, ContactId=c.id);
                if(firstContact ) 
                {
                    ocr.IsPrimary = TRUE;
                    firstContact = FALSE;
                }
                listOCR.add(ocr);
            }
        }
    }
    insert listOCR;
}