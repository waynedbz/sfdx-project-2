trigger linkContact on Opportunity (after insert, after update) {
  //get Ids of accounts
    Set<ID> accountIds = new Set<ID>();
    for(Opportunity opt:Trigger.New){
    accountIds.add(opt.AccountId);
    }
    	//get contacts of accounts
     	list<Contact> cont = new list<Contact>();
     	cont = [select id, AccountId from Contact where AccountId in: accountIds];
    
    //organise contacts
    Map<Id,List<Contact>> contactsByAccount = new Map<ID,List<Contact>>();
    for(Contact c:cont){
        if(contactsByAccount.get(c.AccountId) == null){
            contactsByAccount.put(c.AccountId,new List<Contact>());
        }
        contactsByAccount.get(c.AccountId).add(c);
    }
    // check to see if the Opportunity already has a contact role.  If it does, add to a set of Ids to exclude
    List<OpportunityContactRole> existingOCR = new List<OpportunityContactRole>();
    Set<Id> existingOCRIds = new Set<Id>();
    existingOCR = [select OpportunityId from OpportunityContactRole where OpportunityId in:Trigger.newMap.keySet()]; 
    for(OpportunityContactRole ocr:existingOCR) 
        if(!existingOCRIds.contains(ocr.OpportunityId)) 
        		existingOCRIds.add(ocr.OpportunityId);
    
    //create the OpportunityContactRole objects
    list<OpportunityContactRole> lstOCR = new list<OpportunityContactRole>();
    for(Opportunity opt:Trigger.New){
        if(!existingOCRIds.contains(opt.Id) && contactsByAccount.get(opt.AccountId) != null){
            Boolean firstContact = true;
            for(Contact c: contactsByAccount.get(opt.AccountId)){
                OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId=opt.id, ContactId=c.id);
                if(firstContact) {
                    ocr.IsPrimary = true;
                    firstContact = false;
                }
                lstOCR.add(ocr);
            }
        }
    }
    insert lstOCR;
}