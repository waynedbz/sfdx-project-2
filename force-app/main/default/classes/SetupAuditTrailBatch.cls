global class SetupAuditTrailBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    global String csvColumnHeader;
    global List<String> csvRowValues = new List<String>();
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        //Query all SetupAuditTrail records.
        String query = 'SELECT CreatedDate, CreatedBy.Username, Display, Section, Action, DelegateUser, CreatedById, CreatedBy.Name FROM SetupAuditTrail ORDER BY CreatedDate DESC limit 1';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        //Process retrieved SetupAuditTrail records and format field values.
        /*for(SetupAuditTrail currSetupAudit : (List<SetupAuditTrail>) scope){
String formattedDate = currSetupAudit.CreatedDate.format('M/d/yyyy h:mm:ss a z');
String userName = currSetupAudit.CreatedBy.Username != null ? currSetupAudit.CreatedBy.Username : '';
String displayVal = currSetupAudit.Display != null ? String.valueOf(currSetupAudit.Display).escapeCsv() : '';
String sectionVal = currSetupAudit.Section != null ? currSetupAudit.Section : '';
String delegateUser = currSetupAudit.DelegateUser != null ? currSetupAudit.DelegateUser : '';

String rowStr = formattedDate + ',' + userName + ',' + displayVal + ',' + sectionVal + ','+ delegateUser;
csvRowValues.add(rowStr);
}*/
        
        System.debug(Limits.getLimitHeapSize() - Limits.getHeapSize());
        
        for(Integer i = 0; i < 1000000000; i++){
            if(Limits.getCpuTime() > 55000 || Limits.getHeapSize() > 11000000){
                break;
            }
            String rowStr = 'AAAAAAAA,BBBBBBBB,CCCCCCCC,DDDDDDDD,EEEEEEEE,AAAAAAAA,BBBBBBBB,CCCCCCCC,DDDDDDDD,EEEEEEEE,AAAAAAAA,BBBBBBBB,CCCCCCCC,DDDDDDDD,EEEEEEEE,AAAAAAAA,BBBBBBBB,CCCCCCCC,DDDDDDDD,EEEEEEEE,AAAAAAAA,BBBBBBBB,CCCCCCCC,DDDDDDDD,EEEEEEEE,AAAAAAAA,BBBBBBBB,CCCCCCCC,DDDDDDDD,EEEEEEEE,AAAAAAAA,BBBBBBBB,CCCCCCCC,DDDDDDDD,EEEEEEEE,AAAAAAAA,BBBBBBBB,CCCCCCCC,DDDDDDDD,EEEEEEEE,AAAAAAAA,BBBBBBBB,CCCCCCCC,DDDDDDDD,EEEEEEEE';
            csvRowValues.add(rowStr);
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
        //List<Folder> folders = [SELECT Id, Name FROM Folder WHERE Name = 'Setup Audit Trail Logs'];
        
        //if(!folders.isEmpty()){
            System.debug('ENTER');
            String documentName = 'SetupAuditTrailLog-'+ Datetime.now();//.format('MMM') + Datetime.now().year();
            csvColumnHeader = 'Date, User, Action, Section, Delegate User\n';
            String csvFile = csvColumnHeader + String.join(csvRowValues,'\n');
            
            // Insert the generated CSV file in Document object under "Setup Audit Trail Logs".
            //Document doc = new Document(Name = documentName, Body = Blob.valueOf(csvFile), FolderId = folders[0].Id, Type = 'csv', ContentType='application/vnd.ms-excel');
            //upsert doc;
            
            ContentVersion file = new ContentVersion(
                title = documentName+'.csv',
                versionData = Blob.valueOf( csvFile ),
                pathOnClient = '/'+documentName+'.csv'
            );
            insert file;
        //}
    }
}