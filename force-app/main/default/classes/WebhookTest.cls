// https://testing-dbz-developer-edition.eu14.force.com/services/apexrest/v1/account/
@RestResource(urlMapping='/v1/account/*')
global class WebhookTest {
    @HttpGet
    global static void getAcc() 
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        
        Map<String, String> reqParams = req.params;
        
        if(reqParams != null){
            if(reqParams.get('id') != null && reqParams.get('id')!= ''){
                JSONGenerator jsonRes = JSON.createGenerator(true);
                jsonRes.writeStartObject();
                jsonRes.writeStringField('id', String.valueOf(req.params.get('id')));
                jsonRes.writeEndObject();
                jsonRes.close();
                
                res.responseBody = Blob.valueOf(jsonRes.getAsString());
            }else{
                
                JSONGenerator jsonRes = JSON.createGenerator(true);
                jsonRes.writeStartObject();
                jsonRes.writeStringField('err', 'Id not found');
                jsonRes.writeEndObject();
                jsonRes.close();
                
                res.responseBody = Blob.valueOf(jsonRes.getAsString());
            }
            /*
            if(reqParams.get('name') != null && reqParams.get('name')!= ''){
                JSONGenerator jsonRes = JSON.createGenerator(true);
                jsonRes.writeStartObject();
                jsonRes.writeStringField('name', String.valueOf(req.params.get('name')));
                jsonRes.writeEndObject();
                jsonRes.close();
                
                res.responseBody = Blob.valueOf(jsonRes.getAsString());
            }else{
                
                JSONGenerator jsonRes = JSON.createGenerator(true);
                jsonRes.writeStartObject();
                jsonRes.writeStringField('err', 'Name not found');
                jsonRes.writeEndObject();
                jsonRes.close();
                
                res.responseBody = Blob.valueOf(jsonRes.getAsString());
            }
			*/
        }
    }
}