/**
 * @description       : 
 * @author            : Wayne Solomon
 * @group             : 
 * @last modified on  : 07-24-2020
 * @last modified by  : Wayne Solomon
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   07-24-2020   Wayne Solomon   Initial Version
**/
public with sharing class ShippingService {
    Interfaces.IShipping shipping;

    public ShippingService(Interfaces.IShipping shipping) {
        this.shipping = shipping;
    }

    public void sendParcel(){
        shipping.sendShipment('4223');
    }
}