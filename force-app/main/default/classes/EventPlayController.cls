public with sharing class EventPlayController {
    
    @AuraEnabled(cacheable=true)
    public static List<Account> getSomeAccounts(){
        return [select Id,Name,Type, Lwc_test__c from account
                where (not name like '%altered%')
                and type != null
                limit 10];
    }
}