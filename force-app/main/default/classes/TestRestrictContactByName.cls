@isTest
class TestRestrictContactByName{
    @isTest
    static void myContactInsert(){
        Contact c1 = new Contact(FirstName = 'Joe', LastName = 'INVALIDNAME');
        Contact c2 = new Contact(FirstName = 'Jane', LastName = 'Doe');
        
        test.startTest();
        database.SaveResult result1 = database.insert(c1);
        database.SaveResult result2 = database.insert(c2);
        test.stopTest();
        
        System.assertEquals(result1, result1);
        System.assertEquals(result2, result2);
        
        
        
    }
}