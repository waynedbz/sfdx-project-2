public class OpportunityAfterUpdateTriggerHandler extends TriggerHandlerBase {
	public override void mainEntry(TriggerParameters tp) {
		System.debug('Enter MAIN OpportunityAfterUpdateTriggerHandler');

		process((List<Opportunity>)tp.newList);
	}
	
	private void process(List<Opportunity> listNewOpps) {
		for(Opportunity opp : listNewOpps) {
			Opportunity newOpp = new Opportunity();

			newOpp.Id = opp.Id;
			newOpp.Description = opp.Name + ' - ' + System.now();

			sObjectsToUpdate.put(newOpp.Id, newOpp);
		}
	}

	public override void inProgressEntry(TriggerParameters tp) {
		System.debug('Enter INPROGRESS OpportunityAfterUpdateTriggerHandler');
	}
}