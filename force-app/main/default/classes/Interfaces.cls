/**
 * @description       : 
 * @author            : Wayne Solomon
 * @group             : 
 * @last modified on  : 07-24-2020
 * @last modified by  : Wayne Solomon
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   07-22-2020   Wayne Solomon   Initial Version
**/

public class Interfaces {
    
    public interface IAccounts {
        
    }

    public interface IContacts {
        
    }

    public interface IShipping {
        void sendShipment(String orderNum);
    }
}