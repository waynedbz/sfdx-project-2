public class AccountProcessor {
    
    @future
    public static void countContacts (List<ID> accIDs){
    	List<Account> accs = [select id, Number_of_Contacts__c, (select id from contacts)
                             from Account
                             where id =: accIDs];
        
        if(accs !=null){
            for(Account a : accs){
                a.Number_of_Contacts__c = a.Number_of_Contacts__c + a.contacts.size();
            }
        }
            
    }

}