public class NewCaseListController {
   
    public List<Case> getNewCases(){
        List<Case> lstCases = new List<Case>([select id,CaseNumber from case where status = 'New']);
        
        return lstCases;
    }
}