public class Schemas 
{
	public static void playToken()
    {
        // Create a new account as the generic type sObject
        sObject s = new Account();
        
        System.debug(s);
        
        // Get the sObject describe result for the Account object
        Schema.DescribeSObjectResult dsr = Account.sObjectType.getDescribe();
        
        System.debug(dsr);
        
        // Get the field describe result for the Name field on the Account object
        Schema.DescribeFieldResult dfr = Schema.sObjectType.Account.fields.Name;
        
        System.debug(dfr);
                
        // Get the field describe result from the token
        dfr = dfr.getSObjectField().getDescribe();
        
        System.debug(dfr);
        
        
    }
}