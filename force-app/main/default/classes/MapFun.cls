public class MapFun {
	
    public static void printMapFun()
    {
        Map<String,String> m1 = new Map<String,String>();
        Map<String,String> m2 = new Map<String,String>();
        
        m1.put('Wayne', 'Male');
        m1.put('Chantal', 'Female');
        m1.put('Skye', 'Female');
        m1.put('Hannah', 'Female');
        
        m2.put('Rudolf', 'Male');
        m2.put('Tercia', 'Female');
        m2.put('Wayne', 'Male');
        m2.put('Hayley', 'Female');
        m2.put('Sasha', 'Female');
        
        
        Boolean wayneInMap = false;
        Integer count = 1;
        for(String s : m1.keySet())
        {
            if (m2.containsKey(s))
            {
                wayneInMap = true;
            }
            System.debug(count++);
            
        }
        System.debug(wayneInMap);
        
    }
}