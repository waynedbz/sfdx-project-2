public with sharing class PopupCreateController {
	
    private Account a;
    public String txtStatus{get;set;}
	public String txtAccName{get;set;}
	public String txtAccCity{get;set;}
	public String txtAccPhone{get;set;}

	public PopupCreateController()
	{
		this.txtAccName = '';
		this.txtAccCity = '';
		this.txtAccPhone = '';
	}

    public void submitInputs() {
        this.txtStatus = ApexPages.currentPage().getParameters().get('status');
        this.txtAccName = ApexPages.currentPage().getParameters().get('name');
        System.debug(this.txtAccName);
        this.txtAccCity = ApexPages.currentPage().getParameters().get('city');
        this.txtAccPhone = ApexPages.currentPage().getParameters().get('phone');
        
        this.a  = new Account();
        this.a.Name = this.txtAccName;
        this.a.BillingCity = this.txtAccCity;
        this.a.ShippingCity = this.txtAccCity;
        this.a.Phone = this.txtAccPhone;
        
        try
        {
            insert a;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, 'Account "' + this.a.Name + '" created.'));
        }catch (Exception e)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Record could not be created due to: ' + e));
        }
    }

}