public with sharing class OpportunityAfterInsertTriggerHandler extends TriggerHandlerBase {

    public override void mainEntry(TriggerParameters tp) {
        System.debug('Enter MAIN OpportunityAfterInsertTriggerHandler');
        delete [SELECT id FROM Opportunity where id in: tp.newList];
        return;
	}

	public override void inProgressEntry(TriggerParameters tp) {
        System.debug('Enter INPROGRESS OpportunityAfterInsertTriggerHandler');
        return;
	}
}