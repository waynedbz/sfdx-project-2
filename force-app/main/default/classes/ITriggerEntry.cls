public interface ITriggerEntry {
    void mainEntry(
        String triggerObject,
        Boolean isBefore,
        Boolean isDelete,
        Boolean isAfter,
        Boolean isInsert,
        Boolean isUpdate,
        Boolean isExecuting,
        List<SObject> newList, Map<Id,SObject> newMap,
        List<SObject> oldList, Map<Id,SObject> oldMap
    );

    void inProgressEntry(
        String triggerObject,
        Boolean isBefore,
        Boolean isDelete,
        Boolean isAfter,
        Boolean isInsert,
        Boolean isUpdate,
        Boolean isExecuting,
        List<SObject> newList, Map<Id,SObject> newMap,
        List<SObject> oldList, Map<Id,SObject> oldMap
    );
}