public class OpportunityBeforeUpdateTriggerHandler extends TriggerHandlerBase {
	public override void mainEntry(TriggerParameters tp) {
		System.debug('Enter MAIN OpportunityBeforeUpdateTriggerHandler');
	}

	public override void inProgressEntry(TriggerParameters tp) {
		System.debug('Enter INPROGRESS OpportunityBeforeUpdateTriggerHandler');
	}
}