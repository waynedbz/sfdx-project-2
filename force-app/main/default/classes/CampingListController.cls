public with sharing class CampingListController {
    
        @AuraEnabled
    public static List<Camping_Item__c> getItems() {
        
        // Check to make sure all fields are accessible to this user
        String[] fieldsToCheck = new String[] {
            'Id', 'Name', 'Price__c', 'Quantity__c'
        };
        
        Map<String,Schema.SObjectField> fieldDescribeTokens = 
            Schema.SObjectType.Camping_Item__c.fields.getMap();
        
        for(String field : fieldsToCheck) {
            if( ! fieldDescribeTokens.get(field).getDescribe().isAccessible()) {
                throw new System.NoAccessException();
                //return null;
            }
        }
        
        System.debug('query *** '+[SELECT Id, Name, Price__c, Quantity__c
                FROM Camping_Item__c]);
        
        // OK, they're cool, let 'em through
        return [SELECT Id, Name, Price__c, Quantity__c
                FROM Camping_Item__c];
    }
    
    @AuraEnabled
    public static Camping_Item__c saveItem(Camping_Item__c campingitem) {
        // Perform isUpdatable() checking first, then
        upsert campingitem;
        return campingitem;
    }

}