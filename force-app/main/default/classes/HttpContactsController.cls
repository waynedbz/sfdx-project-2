public class HttpContactsController {
    
    public HttpContactsController(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(Label.ContactEndpointURL);
        req.setMethod('GET');
        
        System.debug(req.getEndpoint());
        
        
        Http http = new Http();
        HttpResponse res = http.send(req);
        String jsonString = res.getBody();
        //System.debug(res.getBody());
                
        List<String> ids = new List<String>();
        JSONParser parser1 = JSON.createParser(jsonString);
       	while (parser1.nextToken() != null) {
            if ((parser1.getCurrentToken() == JSONToken.FIELD_NAME) && (parser1.getText() == 'id')) {
                parser1.nextToken();
                ids.add(parser1.getText());
            }
        }
        
        contacts = new List<WrapContact>();
    
    	for(Integer i=0; i<ids.size(); i++){
        	contact = new WrapContact();
        
        	contact.cId = ids[i];
        	System.debug(contact.cId);
    	}
    }
    
    
    
    public WrapContact contact{get;set;}
    public List<WrapContact> contacts{get;set;}
    
    public class WrapContact{
        public String cId{get;set;}
        public String cName{get;set;}
        public String cAddress{get;set;}
        public String cGender{get;set;}
        public String cEmail{get;set;}
        public String cMobile{get;set;}
    }
}