public class PardotCallout {
    
    static String baseUrl = 'https://pi.pardot.com/api';
    static String email = 'service-ten-x@canpango.com';
    static String password = 'cNVk#j5X';
    static String user_key = 'e0636d2cc3d8c7c1cbba9701b9453dac';
    static String api_key = '';
    
    public static void getPardotApiKey(){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        req.setEndpoint(baseUrl+'/login/version/4/');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setBody('email='+email+'&password='+password+'&user_key='+user_key+'&format=json');
        
        
        res = h.send(req);
        Map<String,Object> jsonRes = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
        
        if (jsonRes.containsKey('api_key')){
            api_key = (String)jsonRes.get('api_key');
            System.debug(api_key);
        }else if (jsonRes.containsKey('err')){
            System.debug(jsonRes.get('err'));
        }
        
    }
    
    public static void getPardotEmail(String email_list_id){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        req.setEndpoint(baseUrl+'/email/version/4/do/read/id/'+email_list_id+'?include_message=false');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setBody('api_key='+api_key+'&user_key='+user_key+'&format=json');
        
        res = h.send(req);
        Map<String,Object> jsonRes = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
        
        EmailData e = (EmailData)JSON.deserialize(res.getBody(), EmailData.class);
        
        System.debug(e);
        
        System.debug(e.email);
        
        System.debug(e.email.id);
        System.debug(e.email.name);
        System.debug(e.email.isOneToOne);
        System.debug(e.email.subject);
        
    }
    
    public static void getPardotEmailClicks(String email_list_id){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        req.setEndpoint(baseUrl+'/emailClick/version/4/do/query?list_email_id='+email_list_id);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setBody('api_key='+api_key+'&user_key='+user_key+'&format=json');
        
        res = h.send(req);
        Map<String,Object> jsonRes = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
        
        EmailClickData ec = (EmailClickData)JSON.deserialize(res.getBody(), EmailClickData.class);
        
        System.debug(ec);
        
        System.debug(ec.result.emailClick);
        
        if(ec.result.total_results>0){
            for(EmailClick e : ec.result.emailClick){
                System.debug(e.id);
                System.debug(e.prospect_id);
                System.debug(e.url);
                System.debug(e.list_email_id);
                System.debug(e.created_at);
                System.debug('**************');
            }            
        }else{
            System.debug('No results found');
        }
        
        
        
    }
    
    // EmailData
    class EmailData{
        public email email {get;set;}
        public String created_at {get;set;}
    }
    
    class Email{
        public String id {get;set;}
        public String name {get;set;}
        public String isOneToOne {get;set;}
        public String subject {get;set;}
    }
    // end EmailData
    
    
    // EmailClickData
    class EmailClickData{
        public result result {get;set;}
    }
    
    class Result{
        public Integer total_results {get;set;}
        public List<EmailClick> emailClick {get;set;} 
    }
    
    class EmailClick{
        public String id {get;set;}
        public String prospect_id {get;set;}
        public String url {get;set;}
        public String list_email_id {get;set;}
        public String created_at {get;set;}
    }
    // end EmailClickData
}