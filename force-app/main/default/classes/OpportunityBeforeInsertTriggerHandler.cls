public with sharing class OpportunityBeforeInsertTriggerHandler extends TriggerHandlerBase {
    
    public override void mainEntry(TriggerParameters tp) {
        System.debug('Enter MAIN OpportunityBeforeInsertTriggerHandler');
        return;
	}

	public override void inProgressEntry(TriggerParameters tp) {
        System.debug('Enter INPROGRESS OpportunityBeforeInsertTriggerHandler');
        return;
	}
}