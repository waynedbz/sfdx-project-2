/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-22-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-22-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class AccountListVf implements Interfaces.IAccounts {

    public List<Account> getAccsWithSharing() {
        return new AccountListVfSharing().getAccs();
    }

    public List<Account> getAccsWithoutSharing() {
        return new AccountListVfWithOutSharing().getAccs();

    }

    private without sharing class AccountListVfWithOutSharing {
        public List<Account>  getAccs() {
            return new AccountListVfSharing().getAccs();
    
        }
    }

    private inherited sharing class AccountListVfSharing {

        public List<Account>  getAccs() {
            return [select id,name from Account order by name asc limit 100 ];
    
        }
    }
}