public class HttpCallOutController {


    public String key {get;set;}
    public String citySearch {get;set;}
    public List<Object> resultList {get;set;}
    public List<LocationWrapper> locations {get;set;}
    public List<WeatherConditionWrapper> conditions {get;set;}
    public String EndpointURL {get;set;}
    
    //final public String accKey = '4TRvWKgfegulQUgIvsGZoPdyqStHm680';
    final public String accKey = 'B28plEHBp1Gl6H0LEJ8ef6nGRzHdhiJk';
    
    public void makeCallout()
    {
        //makeCalloutKeys();
        //makeCalloutWeatherCondition();
    }
    
    public void makeCalloutKeys()
    {
        HttpRequest req = new HttpRequest();
        String jsonResponse = '';
        req.setEndpoint('http://dataservice.accuweather.com/locations/v1/cities/search?apikey='+accKey+'&q='+citySearch.replaceAll('\\s+', '%20')+'');
        req.setMethod('GET');
        EndpointURL = req.getEndpoint();
        // Create a new http object to send the request object
        // A response object is generated as a result of the request  
      
        Http http = new Http();
        HTTPResponse res = http.send(req);
                System.debug(res.getBody());
        locations = getLocationKeys(res.getBody(), citySearch);
    }
    
    public void makeCalloutWeatherCondition()
    {
        HttpRequest req = new HttpRequest();
        String jsonResponse = '';
        req.setEndpoint('http://dataservice.accuweather.com/currentconditions/v1/'+key+'?apikey=+'+accKey);
        
        req.setMethod('GET');
        EndpointURL = req.getEndpoint();
        // Create a new http object to send the request object
        // A response object is generated as a result of the request  
      
        Http http = new Http();
        HTTPResponse res = http.send(req);
                System.debug(res.getBody());
        conditions = getWeatherConditions(res.getBody(), key);
    }
    
        public List<LocationWrapper> getLocationKeys(String res, String keyword)
    {
        JSONParser parser = JSON.createParser(res);
        List<LocationWrapper> locs = new List<LocationWrapper>();
        
        while(parser.nextToken() != null)
        {
            String tempKey = '';
            String tempCountry = '';
            String tempProvince = '';
            
            //System.debug('CurrentToken ***********'+parser.getCurrentToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
            if(parser.getText() == 'Key')
            {
                parser.nextToken();
                tempKey = parser.getText();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                tempCountry = parser.getText();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                tempProvince = parser.getText();
                
                
                locs.add(new LocationWrapper(tempKey,tempCountry,tempProvince));
            }
        }
        return locs;
    }
    
    public List<WeatherConditionWrapper> getWeatherConditions(String res, String key)
    {
        JSONParser parser = JSON.createParser(res);
        List<WeatherConditionWrapper> cons = new List<WeatherConditionWrapper>();
        
        while(parser.nextToken() != null)
        {
            String tempCurrentTime = '';
            String tempWeatherText = '';
            String tempDegreesCelsius = '';
            
            //System.debug('CurrentToken ***********'+parser.getCurrentToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
            if(parser.getText() == 'LocalObservationDateTime')
            {
                parser.nextToken();
                tempCurrentTime = parser.getText();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                tempWeatherText = parser.getText();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                tempDegreesCelsius = parser.getText();
                
                cons.add(new WeatherConditionWrapper(tempCurrentTime,tempWeatherText,tempDegreesCelsius));
            }
        }
        return cons;
    }
    
    public class LocationWrapper
    {
        public String key {get;set;}
        public String country {get;set;}
        public String province {get;set;}
        
        public LocationWrapper(String k, String c, String p)
        {
            key = k;
            country = c;
            province = p;
        }
    }
    
    public class WeatherConditionWrapper
    {
        public String currentTime {get;set;}
        public String weatherText {get;set;}
        public String degreesCelsius {get;set;}
        
    public WeatherConditionWrapper(String t, String w, String c)
        {
            currentTime = t;
            weatherText = w;
            degreesCelsius = c;
        }
    }
    
}