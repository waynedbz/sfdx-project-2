public class GetStates {

	public class RestResponse {
		public List<String> messages;
		public List<Result> result;
	}

	public class Result {
		public String country;
		public String name;
		public String abbr;
		public String area;
		public String largest_city;
		public String capital;
	}

	public RestResponse RestResponse;
	

	
	public static GetStates parse(String json) {
		return (GetStates) System.JSON.deserialize(json, GetStates.class);
	}
}