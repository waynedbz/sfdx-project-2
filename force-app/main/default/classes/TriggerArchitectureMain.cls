public class TriggerArchitectureMain {
    
    public static ITriggerEntry activeFunction = null;

    public static void entry(
        String triggerObject,
        Boolean isBefore,
        Boolean isDelete,
        Boolean isAfter,
        Boolean isInsert,
        Boolean isUpdate,
        Boolean isExecuting,
        List<SObject> newList, Map<Id,SObject> newMap,
        List<SObject> oldList, Map<Id,SObject> oldMap)
    {
        if ( activeFunction != null){

            activeFunction.inProgressEntry(
                triggerObject, 
                isBefore, 
                isDelete, 
                isAfter, 
                isInsert, 
                isUpdate, 
                isExecuting, 
                newList, newMap, oldList, oldMap);
            
            return;
        }

        if (triggerObject == 'Opportunity' && isAfter && isUpdate){

            activeFunction = new TriggerArchitectureClass();

            activeFunction.mainEntry(
                triggerObject,
                isBefore, 
                isDelete, 
                isAfter, 
                isInsert, 
                isUpdate, 
                isExecuting, 
                newList, newMap, oldList, oldMap);
        }

    }
}