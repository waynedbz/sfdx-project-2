public class ContactSearch {
    public static List<Contact> searchForContacts (String conLastName, String conPostalCode){
        List<Contact> myContacts = [select name from contact where LastName=:conLastName and MailingPostalCode=:conPostalCode];
        
        System.debug(myContacts);
        return myContacts;
    }
}