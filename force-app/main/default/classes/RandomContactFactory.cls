public class RandomContactFactory {
    
    public static List<Contact> generateRandomContacts(Integer num,String lastName){
        List<Contact> lstContacts = new List<Contact>();
        for(Integer i = 0; i < num; i++){
            Contact c = new Contact(FirstName = 'FirstName'+i,LastName = lastName);
            lstContacts.add(c);
        }
        
        return lstContacts;
    }

}