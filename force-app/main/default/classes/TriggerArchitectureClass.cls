public class TriggerArchitectureClass implements ITriggerEntry {
    
    public void mainEntry(
        String triggerObject,
        Boolean isBefore,
        Boolean isDelete,
        Boolean isAfter,
        Boolean isInsert,
        Boolean isUpdate,
        Boolean isExecuting,
        List<SObject> newList, Map<Id,SObject> newMap,
        List<SObject> oldList, Map<Id,SObject> oldMap
    ){
        System.debug((List<Opportunity>)newList);
    }

    static Boolean runDebug = false;

    public void inProgressEntry(
        String triggerObject,
        Boolean isBefore,
        Boolean isDelete,
        Boolean isAfter,
        Boolean isInsert,
        Boolean isUpdate,
        Boolean isExecuting,
        List<SObject> newList, Map<Id,SObject> newMap,
        List<SObject> oldList, Map<Id,SObject> oldMap
    ){

        if(!runDebug){
            runDebug=true;
            System.debug((List<Opportunity>)newList);
            
        }
        

    }
    
}