@IsTest
private class TestLinkageClass 
{
    //In this test class we test the trigger that fires after an insertion of an opportunity record
    //Suppose that an opportunity is created, upon it's creation, let the related account consist of numerous contacts
    //Suppose that the account has two contact, one flagged as key contact, one that is not flagged as such
    //We use these contacts to update the opportunity contact role.
    //Key Contact of the related account to the opportunity should be added as Contact roles (Primary)
    static testMethod void createAccount()
    {
        
        Account a = new Account();
        
        //First create the Account
        a.Name = 'Account Corp';
        insert a;
        System.debug('created account');
                

        
        //Then create a Key Contact
        Contact c = new Contact();
        c.LastName = 'Thipe';
        c.AccountId = a.Id;
        c.Key_Contact__c = true;
        insert c;
        System.debug('created primary contact');
        
        //Then create a contact that is not flagged as Key Contact
        Contact con = new Contact();
        con.LastName  = 'Test';
        con.AccountId = a.Id;
        con.Key_Contact__c = false;
        insert con;
        System.debug('created primary contact');
                
        //Now create an opportunity
        Opportunity o = new Opportunity();
        o.AccountId = a.id;
        o.Name = 'Testing Opportunity';
        o.StageName = 'Prospect';
        o.CloseDate = system.today();
        insert o;
        System.debug('created opportunity');

        
        //Only add key contacts to opportunity as Contact Roles
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = c.Id;
        ocr.OpportunityId = o.Id;
        if(c.Key_Contact__c == true)
        {
            ocr.IsPrimary = TRUE;
        }
        ocr.Role = 'Decision Maker';
        insert ocr;
        System.debug('created opportunity contact role for primary');
        

    }
}