@isTest
public class UnitOfWorkTest {

    @isTest
    static void challengeComplete() {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(
            new Schema.SObjectType[] {
                Account.SObjectType,
                Contact.SObjectType,
                Note.SObjectType
            }
        );

        // Do some work!
        for(Integer a=0; a<100; a++) {

            Account acc = new Account(Name = 'UoW Test Name ' + a);

            uow.registerNew(acc);
            for(Integer c=0; c<5; c++) {

                Contact con = new Contact(LastName =  'UoW Test Name ' + c);

                uow.registerNew(con,Contact.AccountId,acc);
                for(Integer n=0; n<1; n++) {

                    Note nt = new Note(Title =  'UoW Test Name ' + n);

                    uow.registerNew(nt,Note.ParentId,con);
                }
            }  
        }
        uow.commitWork();

        System.debug([Select Id from Account].size());
        System.debug([Select Id from Contact].size());
        System.debug([Select Id from Note].size());

        System.assertEquals(100, [Select Id from Account].size());
        System.assertEquals(500, [Select Id from Contact].size());
        System.assertEquals(500, [Select Id from Note].size());
    }
}