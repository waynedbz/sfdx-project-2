@isTest
public class checkOppCon {
    static testMethod void oppContTest()
    {
        Account acct = new Account(name='Test1', Industry = 'Banking');
        insert acct;
        
        Set<Id> contIds = new Set<Id>{};
            Contact[] newContacts = new Contact[]{};
            
            Contact ctc = new Contact(AccountId = acct.Id, lastname='Testing', firstname='Tester', Phone='0115551234', Email='testman@example.co.za' );
            newContacts.add(ctc);
        
        insert newContacts;
        update newContacts;
        
        Opportunity[] newOpp = new Opportunity[]{};
        OpportunityContactRole[] ocr = new OpportunityContactRole[]{};
        
        Opportunity oppt = new Opportunity(AccountId = acct.Id, Name='TestOpp', StageName='Prospecting', Discount_Percent__c = 10, CloseDate=Date.today().addDays(5));
        newOpp.add(oppt);
        
        insert newOpp;
        
        integer counter = 0;
        for(Contact con:newContacts)
       {
           ocr.add(new OpportunityContactRole(ContactId = con.Id, OpportunityId = newOpp.get(counter++).Id));
           contIds.add(con.Id);
       }
       	   insert ocr;
       
        for(Contact con: [Select Id from Contact where Id in: contIds])
        {
            system.assert(true);
        }
    }

}