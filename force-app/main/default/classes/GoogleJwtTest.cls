public class GoogleJwtTest {
    
    private static Map<String,Type>ServiceMapByName = new Map<String,Type>{
        
    };
    
    
    public static void GoogleJwtInitialise()
    {
        JSONGenerator jsonHeader = JSON.createGenerator(true);
        jsonHeader.writeStartObject();
        jsonHeader.writeStringField('alg', 'RS256');
        jsonHeader.writeStringField('typ', 'JWT');
        jsonHeader.writeEndObject();
        jsonHeader.close();
        
        JSONGenerator jsonPayload = JSON.createGenerator(true);
        jsonPayload.writeStartObject();
        jsonPayload.writeStringField('iss', 'srv-salesforce@s2g-jwt-auth.iam.gserviceaccount.com');
        jsonPayload.writeStringField('scope', 'https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/spreadsheets https://www.googleapis.com/auth/youtube https://mail.google.com/ https://www.googleapis.com/auth/cloud-platform');
        //jsonPayload.writeStringField('sub', 'wsolomon30@gmail.com');
        jsonPayload.writeStringField('aud', 'https://www.googleapis.com/oauth2/v4/token');
        jsonPayload.writeNumberField('exp', datetime.now().addHours(1).getTime()/1000);
        jsonPayload.writeNumberField('iat', datetime.now().getTime()/1000);
        jsonPayload.writeEndObject();
        jsonPayload.close();
        
        string base64Header = encodingUtil.base64Encode(blob.valueOf(jsonHeader.getAsString()));
        string base64Payload = encodingUtil.base64Encode(blob.valueOf(jsonPayload.getAsString()));
        
        String signatureInput = base64Header + '.' + base64Payload;
        String key = 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC826LQGPVG4PD26K42JsXZpJKtqQSc0du6KsElBP4/cd43tm16Flj3cMVmeuiHTvYbXqwB+G+b4g10iTmIDSXV1AqhZcPRL/ILPDwbBUIBeALmXmMVXMNtOVY1rC8kBmFhbSRI6g1p9AsAgA/CCv5sQvCm5PphNnkU1Ux/FphShZAoz79FkSSJqIMyUr/no/2Ziw0m4QqK9Q6p5s0mfwgHNZGHWztH5CC8FogNW1th8ASwum1qzlGIMPizVFsRThmyjLiEH9R8SiwfEieu3KP4YdaOrg2P8pHP6sLquGHAiD95VpkJgmZVTjk7VxIjE9N4dA2WRy4aH9hchVz1u7uFAgMBAAECggEAFqYSbvlnROJKE5pQ9i6NMK4zZv8rjfD+9GMXOazRHuO1fiHzHB6c+cO88K7RvxRhoGOopi3YmK2x5mVkkGPfQzcENVnic25YDS/il2z4fvuZAW6VXGT9DNbA93vYRW48dHYl0Va8WBYL0boJ6b/uxiX6/VDdlBX3zF9OJrmbd93Wkiv2G5QxU8bWCLb1eQkYWrJif9JSeOhyh4yKb0XhSv6yqLkTjfc/Cujg1dm2yK97qNlS14q3f+hbOAmH53CSAE8b4ngo2VoTSzV6fDvkdJmEoMgzA+eyv5JS/fh7EWzilsRNA+H0GHo9k9GJFM+1mx+0FWP8Nhr7zgUT3+2bkQKBgQD3bSSqJtEaE1bONkKRz1QYdH25w5xh4CcGwKtefRZEiwUsBuQNX2TAVEWBY9Oprq2/lnK+W6kqbAnGjI8Ap2TAVlguEWg4vNcegMeqUQSU7Ob0t5vD1iyn8p9SBHxSyeRT5Cwtf2A4BmR8fuMvIFHwSzr4CW9340tXC5A9tyMG3QKBgQDDZvKFKqloBsEst8KFrm83ejQxr21t8tcrXaidQxtBm6+/iJWoYLHlQwdp0gP36ZzoHWx76L2jMGrouKL92EyMlWoZZ36lUttPw0zcdCjppwMsKOXmHfEEhA+rUvHXfw22usYSnKbY+MPrWEYPp980dzNHk+iUraWF3CRGBXc4yQKBgQCu73v80XUF+qZlJffrH6tVnwdFUAhBKzpsi8yeAdK8o44EktqqmceiM/LwiKQeVGKEEtsNu8DfPYOr3bU9wRx64JF4ANavRtTRvDtvL9kNcnHYk/iNfeAU3nXGh2VfI19L8QdLI8Pj2KEzZIM7zp4gRxBqTkGBgjLCWGqjN9iMAQKBgGy2pAtu1t2nlGY6vqD9kab9+HgEMA/R4eCgtST5pvpzke4wIYOSeKcjCcbYL5kIgmGprSerL4bRc0mTICu24a/9mRAVQqLcXGQeNm8YuYJ1hxBUmMPUuLlYeBdLiH3qlEir1XOZcEKa9PXFIQWo0kgwYXlbUzUU/1hgSFYkyFXxAoGARscfhXTuoP5RPz5D9KvDPuvm5yuGuXZRO1BQTyxH78EgAsriWsznKKSuOjFttkseUrwkZvhCIrQE0idsq90VUHcpKJ8nj9xN5wbafcK+drzzJdQ5wvdlVsirhgJMhldZF/HKbsmA/ciwhhhrw62lclRLjo5PblXaDBzxk92j0zM=';
                        
        Blob privateKey = EncodingUtil.base64Decode(key);
        Blob signature = Crypto.sign('RSA-SHA256', Blob.valueOf(signatureInput), privateKey);
        string base64Signature = encodingUtil.base64Encode(signature);
        
        String jwt = base64Header + '.' + base64Payload + '.' + base64Signature;        
        
        
        //********************************************
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        
        String grant_type = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
        grant_type = EncodingUtil.urlEncode(grant_type, 'UTF-8');
        
        
        req.setEndpoint('https://www.googleapis.com/oauth2/v4/token');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setBody('grant_type='+grant_type+'&assertion='+EncodingUtil.urlEncode(jwt,'UTF-8'));
        
        res = new Http().send(req);
        
        System.debug(res);
        System.debug(res.getBody());
    }
}