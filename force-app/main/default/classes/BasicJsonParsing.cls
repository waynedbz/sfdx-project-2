public class BasicJsonParsing {
    
    String strJson = '{"Contact":['+
                        '{"ID":"1","FirstName":"Wayne","LastName":"Solomon", "Region":{"ID":"WC"}}, '+
                        '{"ID":"2","FirstName":"Chantal","LastName":"Johnson"}'+
                     ']}';
    
    List<cContact> con = new List<cContact>();
    public List<cCity> cities {get;set;}
    List<String> lstFirstNames = new List<String>();
    List<String> lstLastNames = new List<String>();
    List<String> lstRegionIds = new List<String>();
    
    public BasicJsonParsing()
    {
        //manuallyStream();
        //manuallyStreamLarge();
        whileStreamLarge();
        //whileStream();
        //singularTokenStream();
        //singularValueStream();

    }
    
    // THIS IS THE WINNING METHOD!!!!!!
    // *********************************
    // *********************************
    // *********************************
    public void whileStreamLarge()
    {
        JSONParser parser = JSON.createParser(largeString);
        cities = new List<cCity>();
        
        while(parser.nextToken() != null)
        {
            String tempKey = '';
            String tempCountry = '';
            
            //System.debug('CurrentToken ***********'+parser.getCurrentToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
            if(parser.getText() == 'Key')
            {
                parser.nextToken();
                tempKey = parser.getText();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();parser.nextToken();
                tempCountry = parser.getText();
                
                
                cities.add(new cCity(tempKey,tempCountry));
            }
        }
        for(cCity c : cities)
        {
            System.debug('Record '+c);
        }
    }
    
    
    public void whileStream()
    {
        JSONParser parser = JSON.createParser(strJson);
        while(parser.nextToken() != null)
        {
            if(parser.getText() == 'FirstName')
            {
                parser.nextToken();
                lstFirstNames.add(parser.getText());
            }
            if(parser.getText() == 'LastName')
            {
                parser.nextToken();
                lstLastNames.add(parser.getText());
            }
            if(parser.getText() == 'Region')
            {
                parser.nextToken();
                parser.nextToken();
                parser.nextToken();
                lstRegionIds.add(parser.getText());
            }
            System.debug('CurrentToken ***********'+parser.getCurrentToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        }
        
        System.debug(lstFirstNames);
        System.debug(lstLastNames);
        System.debug(lstRegionIds);
    }
    
    public void singularTokenStream()
    {
        JSONParser parser = JSON.createParser(strJson);
        System.debug('NextToken ***********'+parser.nextToken());
        System.debug('NextToken ***********'+parser.nextToken());
        System.debug('CurrentToken ***********'+parser.getCurrentToken());
        parser.skipChildren();
        System.debug('CurrentToken ***********'+parser.getCurrentToken());
    }
    
     public void singularValueStream()
    {
        JSONParser parser = JSON.createParser(strJson);
        System.debug('NextValue ***********'+parser.nextValue());
        System.debug('NextValue ***********'+parser.nextValue());
        System.debug('CurrentToken ***********'+parser.getCurrentToken());
        parser.skipChildren();
        System.debug('CurrentToken ***********'+parser.getCurrentToken());
    }
    
    public void manuallyStream()
    {
        JSONParser parser = JSON.createParser(strJson);
        System.debug('CurrentToken ***********'+parser.getCurrentToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
    }
    
    public class cContact
    {
        public String FirstName = '';
        public String LastName = '';
    }
    
    public class cCity
    {
        public String key {get;set;}
        public String country {get;set;}
        
        public cCity(String k, String c)
        {
            key = k;
            country = c;
        }
    }
    
    public void manuallyStreamLarge()
    {
        JSONParser parser = JSON.createParser(largeString);
        System.debug('CurrentToken ***********'+parser.getCurrentToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
        System.debug('NextToken+1 ***********'+parser.nextToken());System.debug('GetTokenText ***********'+parser.getText());System.debug('-----------------');
    }
    
    
    String largeString = '[ '+
  '{'+
   ' "Version": 1,'+
   ' "Key": "306633",'+
   ' "Type": "City",'+
  '  "Rank": 20,'+
  '  "LocalizedName": "Cape Town",'+
  '  "EnglishName": "Cape Town",'+
  '  "PrimaryPostalCode": "",'+
  '  "Region": {'+
   '   "ID": "AFR",'+
   '   "LocalizedName": "Africa",'+
   '   "EnglishName": "Africa"'+
   ' },'+
   ' "Country": {'+
   '   "ID": "ZA",'+
   '   "LocalizedName": "South Africa",'+
   '   "EnglishName": "South Africa"'+
        ' },'+
        
    '"AdministrativeArea": {'+
      '"ID": "WC",'+
     ' "LocalizedName": "Western Cape",'+
    '  "EnglishName": "Western Cape",'+
    '  "Level": 1,'+
    '  "LocalizedType": "Province",'+
    '  "EnglishType": "Province",'+
    '  "CountryID": "ZA"'+
        ' }},'+
   '{'+
   ' "Version": 1,'+
   ' "Key": "406633",'+
   ' "Type": "City",'+
  '  "Rank": 20,'+
  '  "LocalizedName": "Cape Town",'+
  '  "EnglishName": "Cape Town",'+
  '  "PrimaryPostalCode": "",'+
  '  "Region": {'+
   '   "ID": "AFR",'+
   '   "LocalizedName": "Africa",'+
   '   "EnglishName": "Africa"'+
   ' },'+
   ' "Country": {'+
   '   "ID": "ZA",'+
   '   "LocalizedName": "South America",'+
   '   "EnglishName": "South America"'+
        ' }}'+
    '  ]';
    
}