public class AccountTriggerHelper
{
    @future(callout=true)
    public static void notinyxternalSystem (String accountId)
    {
        Account acc = [Select id, name from Account where Id = :accountId];
        //Instantiate a new http object
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://example.org/restService');
        req.setMethod('POST');
        req.setBody(JSON.serialize(acc));
        HttpResponse res = h.send(req);
    }
}