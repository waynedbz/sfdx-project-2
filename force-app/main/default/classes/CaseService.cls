public class CaseService {

    public static void closeCases (Set<Id> caseIds, String closeReason) {
        
        List<Case> cases = [select Id from Case where Id in :caseIds];
        
        List<Case> casesToUpdate = new List<Case>();
        
        for(Case cs : cases) {
            
                casesToUpdate.add(new Case(id=cs.Id,Reason=closeReason,Status='Closed'));
        }
        
         upsert casesToUpdate;
    }
}