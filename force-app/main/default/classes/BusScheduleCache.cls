public class BusScheduleCache {

    private Cache.OrgPartition part;
    
    public BusScheduleCache() {
        this.part =  Cache.Org.getPartition('local.BusSchedule');
    }

    public void putSchedule(String busLine, Time[] schedule){

        this.part.put('busLine', schedule);
    }

    public Time[] getSchedule(String busLine){
        return  ((Time[])this.part.get('busLine')!=null)?(Time[])this.part.get('busLine'):new List<Time>{Time.newInstance(08, 00, 0, 0),Time.newInstance(17, 00, 0, 0)};
    }
}